document.documentElement.style.fontSize =
  (document.documentElement.clientWidth / 750) * 100 + 'px'
window.onresize = function () {
  document.documentElement.style.fontSize =
    (document.documentElement.clientWidth / 750) * 100 + 'px'
}

document.addEventListener('plusready', function () {
  var webview = plus.webview.currentWebview()
  plus.key.addEventListener('backbutton', function () {
    webview.canBack(function (e) {
      if (e.canBack) {
        webview.back()
      } else {
        webview.close()
        // hide,quit
        // plus.runtime.quit()
      }
    })
  })
})
