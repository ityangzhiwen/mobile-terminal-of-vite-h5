import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
  {
    path: '/contract',
    component: () =>
      // import(/* webpackChunkName: "homeChunk" */ '@/pages/Contract.vue') // 不起效
      import('@/pages/Contract.vue')
  },
  {
    path: '/privacy',
    component: () =>
      // import(/* webpackChunkName: "homeChunk" */ '@/pages/Contract.vue') // 不起效
      import('@/pages/Privacy.vue')
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/contract'
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
