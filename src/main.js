import { createApp } from 'vue'

import App from './App.vue'
import '@/common/reset.scss'
import '@/common/px2rem'
import './style.css'

import router from './router'

const app = createApp(App)
app.use(router)
app.mount('#app')
