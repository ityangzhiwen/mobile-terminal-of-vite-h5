import { defineConfig, loadEnv } from 'vite'
import { resolve } from 'path'

import vue from '@vitejs/plugin-vue'
// import { manualChunksPlugin } from 'vite-plugin-webpackchunkname'
import myPlugin from './zip'
console.log('123456789', loadEnv('develop', process.cwd(), ['VITE_', 'VENUS_']))

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    myPlugin()
    //  manualChunksPlugin()
  ],
  base: './',
  server: {
    host: '0.0.0.0'
  },
  resolve: {
    alias: {
      // 键必须以斜线开始和结束
      '@': resolve(__dirname, './src')
    }
  },
  css: {
    preprocessorOptions: {
      // 全局样式引入
      scss: {
        additionalData: '@import "./src/common/variables.scss";',
        javascriptEnabled: true
      }
    }
  }
})
